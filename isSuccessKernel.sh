#!/usr/bin/bash
if [ -z "$1" ]; then
    echo "No argument. One argument, a directory, isrequired."
    exit 1
fi
if [ ! -d "$1" ]; then
    echo "Not a directory: $1"
    exit 1
fi
expected=`head -1 $1/frcheck.out  | awk '{print $2}'`
found=`tail -1 $1/frcheck.out  | awk '{print $1}'`
machine=`grep '^Machine ' $1/machine.ad 2>/dev/null | awk -F\" '{print $2}'  `
cpu=`grep '^OSG_CPU_MODEL ' $1/machine.ad 2>/dev/null | awk -F\" '{print $2}'  `
kernel=`grep '^OSG_OS_KERNEL ' $1/machine.ad 2>/dev/null | awk -F\" '{print $2}'  `
if [ "$expected" = "$found" ]; then
    echo "YES     $machine   $kernel"
    exit 0
else
    echo "NO      $machine   $kernel"
    exit 1
fi
