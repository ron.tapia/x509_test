#!/usr/bin/bash
if [ -z "$1" ]; then
    echo "No argument. One argument, a directory, isrequired."
    exit 1
fi
if [ ! -d "$1" ]; then
    echo "Not a directory: $1"
    exit 1
fi
expected=`head -1 $1/frcheck.out  | awk '{print $2}'`
found=`tail -1 $1/frcheck.out  | awk '{print $1}'`
if [ "$expected" = "$found" ]; then
    echo YES
    exit 0
else
    echo NO
    exit 1
fi
