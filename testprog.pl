#!/usr/bin/env perl
use strict;
use warnings;
use FileHandle;
use Cwd qw(getcwd);

printf("---------------------------\n");
printf("Start\n");
system("echo 'Date: ' `date`");
printf("---------------------------\n");
printf("Hostname: " . `hostname -f`);
if (@ARGV) {
    printf("ARGS: (\"%s\")\n", join("\", \"", @ARGV))
} else {
    printf("ARGS: ()\n");
}
my($outDir) = "unknown";
if (defined($ARGV[0])) {
    $outDir = $ARGV[0];
}
system("mkdir $outDir");
printf("PWD: " . `pwd`);
my($rh) = "UNKNOWN";
if (-f '/etc/redhat-release') {
    my($rh) = `cat /etc/redhat-release`;
    chomp($rh);
}
printf("redhat release: %s\n", $rh);


###
###
###
printf("---------------------------\n");
my($initialX509Proxy) = $ENV{'X509_USER_PROXY'};
if (defined($initialX509Proxy)) {
    printf("Start env: X509_USER_PROXY=%s\n", $initialX509Proxy);
} else {
    printf("Start env: X509_USER_PROXY is not set\n", $initialX509Proxy);
}

if (-f $initialX509Proxy) {
    printf("x509 proxy file exists: %s\n", $initialX509Proxy);
    writeX509($initialX509Proxy, $outDir);
    my($command) = sprintf("X509_USER_PROXY=%s  ls -al /cvmfs", $initialX509Proxy);
    printf("--------------\n");
    printf("COMMAND: %s\n", $command);
    system($command);
    printf("--------------\n");
    $command = sprintf("X509_USER_PROXY=%s  ls -al /cvmfs/ligo.osgstorage.org", $initialX509Proxy);
    printf("COMMAND: %s\n", $command);
    printf("--------------\n");
    $command = sprintf("X509_USER_PROXY=%s  ls -al /cvmfs/ligo.osgstorage.org/frames/O3", $initialX509Proxy);
    printf("COMMAND: %s\n", $command);    
    system($command);
    printf("--------------\n");
    $command = sprintf("ls -al %s", $initialX509Proxy);
    printf("COMMAND: %s\n", $command);    
    system($command);
    printf("--------------\n");    
} else {
    printf("x509 proxy file DOES NOT exist: %s\n", $initialX509Proxy);
}

###
###
###
writeEnv($outDir);
writeDf($outDir);
writeIfconfig($outDir);
writeFree($outDir);
writeFind($outDir);
writeTokens($outDir);
system(sprintf("cp %s %s", $ENV{'_CONDOR_MACHINE_AD'}, getFname('machine.ad', $outDir)));
system(sprintf("cp %s %s", $ENV{'_CONDOR_JOB_AD'}, getFname('job.ad', $outDir)));
system(sprintf("ls -al /run/user > %s", getFname("ls.run.user.txt", $outDir)));
writeCpuInfo($outDir);
runFrCheck($outDir, $initialX509Proxy);

printf("---------------------------\n");
printf("END\n");
system("echo 'Date: ' `date`");
printf("---------------------------\n");


#readAndSleep();


exit(0);
###
### 
###
sub writeX509 {
    my($f) = shift;
    my($outDir) = shift;
    my($fname) = getFname("x509_cert", $outDir);
    system("cp $f $fname");
}
sub getFname {
    my($n)    = shift;
    my($outDir) = shift;
    return sprintf("%s/%s", $outDir, $n);
}

sub writeEnv {
    my($outDir) = shift;
    my($fname) = getFname('env.txt', $outDir);
    system("env > $fname");
}

sub writeDf {
    my($outDir) = shift;
    my($fname) = getFname('df.txt', $outDir);
    system("df -h > $fname");
}

sub writeIfconfig {
    my($outDir) = shift;
    my($fname) = getFname('ifconfig.txt', $outDir);
    if ( -f '/sbin/ifconfig') {
        system("/sbin/ifconfig -a > $fname");
    } else {
        system("touch $fname");
    }
}

sub writeFree {
    my($outDir) = shift;
    my($fname) = getFname('free.txt', $outDir);
    if (-f '/usr/bin/free') {
        system("/usr/bin/free > $fname");
    } else {
        system("touch $fname");
    }
}

sub writeCpuInfo {
    my($outDir) = shift;
    my($fname) = getFname('cpuinfo.txt', $outDir);
    system("cat /proc/cpuinfo > $fname");
}

sub writeFind {
    my($outDir) = shift;
    my($fname) = getFname('find.txt', $outDir);
    system("find . > $fname");    
}

sub runFrCheck {
    my($outDir) = shift;
    my($certFile) = shift;
    my($frameFile) = "/cvmfs/ligo.osgstorage.org/frames/O3/hoft/H1/H-H1_HOFT_C00-12692/H-H1_HOFT_C00-1269264384-4096.gwf";
    my($command) = sprintf("X509_USER_PROXY=%s ls -al %s > %s 2>&1", $certFile, $frameFile, getFname('lsframe.out', $outDir));
    system($command);
    $command = sprintf("echo 'Expecting: ad95bd2b2da3e85e473ae327b604d421' > %s", getFname('frcheck.out', $outDir));
    system($command);
    $command = sprintf("X509_USER_PROXY=%s head -c 4K %s | md5sum >> %s", $certFile, $frameFile, getFname('frcheck.out', $outDir));
    printf("RUNNING command: %s\n", $command);
    my($exitCode) = system($command);
    if ($? == -1) {
        print "command failed to execute\n";
    } elsif ($? & 127) {
        printf "child died with signal %d, %s coredump\n",
        ($? & 127),  ($? & 128) ? 'with' : 'without';
    } else {
        printf "child exited with value %d\n", $? >> 8;
    }
    printf("FINISHED: %s\n", $command);
}

sub writeTokens {
    my($outDir) = shift;
    my($credDir) = $ENV{'_CONDOR_CREDS'};
    my($fh) = FileHandle->new($outDir . "/tokens.txt", "w");
    if (!defined($credDir)) {
        printf($fh "_CONDOR_CREDS is not defined.\n");
        $fh->close();
        return -1;
    }
    if (! -d $credDir) {
        printf($fh "_CONDOR_CREDS is not a directory.\n");
        $fh->close();
        return -1;
    }
    my($dh);
    opendir($dh, $credDir);
    my($f);
    my($th);
    my($line);
    my($tokenPath);
    while ($f = readdir($dh)) {
        $tokenPath = sprintf("%s/%s", $credDir, $f);
        if (-f $tokenPath) {
            printf($fh "===================================\n");
            printf($fh "FILE: %s\n", $f);
            printf($fh "===================================\n");
            $th = FileHandle->new($tokenPath, "r");
            while ($line = <$th>) {
                printf($fh "%s", $line);
            }
            $th->close();
        }
    }
}


sub runDataFindCheck {
    my($outDir) = shift;
    my($outFile) = getFname('datafind.out', $outDir);
    my($command) = sprintf("unset X509_USER_PROXY && /cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/envs/igwn/bin/gw_data_find -r datafind.ligo.org:443 --show-observatories > %s 2>&1", $outFile);
    printf("RUNNING command: %s\n", $command);
    my($exitCode) = system($command);
    if ($? == -1) {
        print "command failed to execute\n";
    } elsif ($? & 127) {
        printf "child died with signal %d, %s coredump\n",
        ($? & 127),  ($? & 128) ? 'with' : 'without';
    } else {
        printf "child exited with value %d\n", $? >> 8;
    }
    printf("FINISHED: %s\n", $command);
}

sub readAndSleep {

    my($fh) = FileHandle->new("/cvmfs/ligo.osgstorage.org/frames/O3/hoft/H1/H-H1_HOFT_C00-12692/H-H1_HOFT_C00-1269276672-4096.gwf", "r");
    
    printf("Sleeping...\n");
    sleep(3000);
    printf("Finished Sleeping...\n");

}
