#!/usr/bin/perl -w
use strict;
my($line);
my($h, $s);
my($status);
while ($line = <>) {
    if ($line =~ /^(\S+)\s+(\S+)\s+(\S+)\s*$/) {
        $s = $2;
        $h = $3;
        if (!defined($status->{$h})) {
            $status->{$h} = {'YES' => 0, 'NO' => 0};
        }
        $status->{$h}->{$s} = $status->{$h}->{$s} + 1;
    }
}

my(@hosts) = keys(%{$status});
@hosts = sort {(($status->{$a}->{'YES'} > 0) <=> ($status->{$b}->{'YES'} > 0)) || (($status->{$b}->{'NO'} > 0) <=> ($status->{$a}->{'NO'} > 0)) || ($a cmp $b)} @hosts;

my($l) = 0;
my($t);
for $h (@hosts) {
    $t = length($h);
    if ($t > $l) {
        $l = $t;
    }
}

for $h (@hosts) {
    printf("%-${l}s   %d    %d\n", $h, $status->{$h}->{'YES'}, $status->{$h}->{'NO'});
}
