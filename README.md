### Test procedure

Need to edit and source env.sh. Need to set `ACCT_USER` and `ACCT_GROUP`.

```
ligo-proxy-init albert.einstein
condor_submit test-multi-x509.sub
```

The testprog.pl script has a sleep statement. The sleep statement can
be commented for a quick test or uncommented in order to have time
to `condor_ssh_to_job` and explore with maybe an strace on
`cvmfs-x509-helper`.

The results can be summarized with commands like:
```
    for i in *-out-*; do echo "$i  "  `./isSuccess.sh $i`; done 2>&1 | tee success.out.txt
```

and

```
   for i in *-out-*; do echo "$i  "  `./isSuccessKernel.sh $i`; done 2>&1 | tee success.kernel.out.txt
```

These summaries can be pretty printed with a command like:
```
   cat success.out.txt | ./parseSuccess.pl
```
